---
title: Character control - delegation vs direct control
date: 2017-10-23 14:09:49
tags:
 - Project Lockdown
 - Super Smash Bros. Melee
 - Game Design
---

Something incredibly important about just about each and every game is that it plays smoothly. Let's talk more specifically about character control. What makes for smooth control, and such questions.

I'd say that character control is a spectrum. On one end, you have games where you very directly control your character, for example retro platformers. You press jump, your character jumps. You press right, your character goes right. On the other hand you've got strategy games and the like, where your control is more of delegation. You tell unit X to go do thing Y while you do something else. This is most common in strategy games, which have you acting as an overseeing commander. In the middle, you have MOBAs among others. You only control one character and you give it pretty direct input, but some things are handled on their own. They implement pathfinding so that your character never just walks into a wall. Attacks and abilities are targeted, and your character will automatically walk close enough to the target to use it, and suffers no risk of missing (unless it's a purely statistical chance of missing). You, being the twelve year old League of Legends fanboy that you are, will exclaim that League of Legends *akschully* has a lot of "skillshots", abilities aimed manually. And you're right, but they're still very delegative actions.

Let's compare the figurative character's actions and your actions when you cast a skillshot aimed at a certain area.

Your character receives word through telegram that you demand they cast their spell upon a specific coordinate. C. Haracter determines where this spot is, gauges the distance the distance to this spot and uses just the right amount of wizardry to cast the spell there.

You, on the other hand, never really cast a spell anywhere. You just said that you wanted the spell to be cast there and didn't really have to perform any of the steps between.

Let's take another example, a far better one: Yoshi's Up-B in Super Smash Bros. Melee. Yoshi's Up-B has two parameters: how long you hold B, as well as your control stick's horizontal position. When you press Up-B, the strength of the throw is determined by how long you hold it before releasing. A short tap will barely send the egg anywhere, while holding it for the maximum period can harass your opponent across the entire stage. The horizontal position is biased in Yoshi's forward direction. A neutral horizontal position will send the egg flying somewhat forward. You'll need to tilt the stick very in a very specific backwards angle in order to achieve a throw straight upwards.

All in all, Yoshi's egg effectively travels in an arc from point A to B in X time. MOBA skillshots will usually do the same, with the only parameters being the targeted area or direction. Yet Yoshi's egg is so much more direct, as there's a strong tie between your precise actions and the character's actions. This is a very pure form of character control. Ye ole MOBA is just a simple delegation. I think it goes without saying that a good egg throw takes a lot more practice and feels so much better than a MOBA skillshot.

This is one of my major gripes with Battlerite. Despite being very directly controlled overall, these targeted spells just feel out of place and all too simplified. Project Lockdown will by all means face the same issues, and hold to charge mechanics are honestly pretty boring if you're gonna use it for each and every projectile in game. Since bombs are so monotone in Spiral Knights, let's try to make an interesting lobbed bomb instead!

First off, the charge mechanic: bomb can be charged as in Spiral Knights, but we can drop it early and it would either be a dud (but the enemy would not know that) or a weaker but functioning bomb, as in Spiral Knights' early days. When you release the bomb, you'd place it as usual. But using the secondary fire button would instead initiate a throw. Your character goes full American handegg throw and locks in your direction. Like Yoshi's egg, longer hold equates to longer distance thrown. Unlike Yoshi's egg, this is a bomb and as far as I know, you prime your bombs before throwing them, unless you're feeling generious. While locked in on the throw, any directional input will, instead of changing direction fully, give the throw a twist up to a certain limit. Thus, you could do some tricky, curved throws that might catch your enemies off-gaurd. Finally, you wouldn't have to throw until you release. Having the bomb blow up in your hand would be a very real option.

And so, this bomb is a load of fun and has very real execution skill:

* You need to time it for the correct distances.
* You would have to learn the appropriate inputs needed for curved throws.
* You can play mindgames with the enemy, using curved throws, charges held for long times, and quickly deployed duds.
* There's more commitment than a plain instantly cast spell. You have to charge the bomb and then choose between placing it and throwing. Once you've opted to throw it, your direction is pretty limited.

Some more bombing ideas that are out of the scope of this post:

* Deployment method: placing, throwing, rolling, *eating?*
* Trigger method: timer, proximity, remote triggering

There's just so much you could do here!

I also recommend reading the [Smash Wiki article on the input buffer](https://www.ssbwiki.com/Buffer). It discusses what an input buffer is, some of its advantages and disadvantages, and its competitive implications.
