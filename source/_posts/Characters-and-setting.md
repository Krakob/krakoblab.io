---
title: Characters and setting
date: 2017-07-24 14:37:14
tags:
 - Project Lockdown
 - Spiral Knights
 - Guild Wars 2
categories:
 - Project Lockdown
---
# Characters
Something I've always liked about Spiral Knights characters is that they're so anonymous, to the point that they're inseparable from the player themselves, in my eyes. Games generally let you use a character (e.g. Final Fantasy X) with predetermined appearance, dialogue, and behaviour, or create your own with some settings (e.g. Spiral Knights).

In recent times, even these kinds of characters are being more and more predetermined. For example, in Guild Wars 2, your character has their own storyline, determined by your race and a few choices within the storyline. This does of course make the character feel more alive, but suddenly it's not your character anymore. It's the writers' character. The voice actor's character.

On the contrary, Spiral Knights did very little of this. In its early days, before missions were added to the game, there was almost no dialogue wherein your character spoke. Nor was there a face, a sex, anything. Just you, the player. They say that if you give a person a mask, they'll reveal their true self, and this is exactly what Spiral Knights does. Yet Guild Wars 2 fails.

The underlying reason is of course that a mask implies anonymity. Spiral Knights gives anonymity. Guild Wars 2 gives the player a fake persona. This fake persona allows the player to blend with their character. In Spiral Knights, there is no character to blend with.

Moving on to this question in Project Lockdown, there's also development resources to account for in this kind of choice. Most MOBAs have a set of heroes with over the top personalities, voice acting, a bit of lore behind them, and so on. Using the Spiral Knights model would have a lot of advantages over this:

 * One player model and accompanying animations fit for all characters
 * No voice acting needed! This also saves the player from clutter in the sound stage and hearing cheesy puns over and over again for hundreds of games.
 * No individual lore needed. While this can be pulled off (e.g. Team Fortress 2), it's usually nothing more than a few lines of text that you read twice at best, and it fails to establish the character well either way.
 * No crappy psychopath magic girl character! Every MOBA seems to have one and while the character might be fun to play, each game also becomes a matter of tolerating the same three voice lines about playful murder by a woman who's probably closing in on her 30's. Go away, Pearl! >:^(

# Setting
Building on the concept of almost identical characters with no sex and, in the context of this game, really just existing for the purpose of competing in lethal combat with each other, I'm gonna have to say they're all clones. For what purpose? Betting, of course!

Spiral Knights' PvP is basically an underground fight ring hosted by King Krogmo, an almost Jabba The Hutt-like character. He's an obese, frog-like Devilite who mostly seems to sit around and watch people fight.

As a ~~man of culture~~ socialist who likes zesty memes, this naturally brings thoughts to the various 4chan greentext fictions about ancapistan (or other similar things), which detail the *wonderful* life under unregulated capitalism.

![Ron Paul's Libertarian Paradise](https://i.imgur.com/0onGR6J.jpg Ron Paul's Libertarian Paradise)

The situation is of course that the frogs, being the most intelligent race on the planet, started using the inferior races to their advantage. Pitting the humanoid creatures against each other became popular not long after the dawn of frog-civilisation. As society progressed and cloning was perfected, it became more efficient to clone the humanoids instead of breeding them.
