---
title: Abilities vs Mechanics
date: 2017-07-17 08:35:53
tags:
 - Project Lockdown
 - Game Design
 - Spiral Knights
 - Overwatch
 - Team Fortress 2
categories:
 - Project Lockdown
---
One thing I've been torn about in games lately is the introduciton of abilities everywhere. In essence, you press a button that makes you do a cool thing and then it goes on cooldown for X seconds. On one hand, I love class based games (no war but the class war amirite). On the other hand, I've always felt like abilities are a cheap way out, either to make a game feel more interesting, or because it's easier to implement than fully fledged mechanics. So what do I mean by mechanic in this case?

Let's take a clear, recent example: Soldier in Team Fortress 2 vs Pharah in Overwatch. Soldier, based on old arena shooters, was designed around being able to rocket jump. All he has is his Rocket Launcher, a sidearm (possibly a shotgun, but most are either passive or activated buffs), and a melee weapon. Let's disregard the latter two and draw paralells to Pharah:

* Soldier's primary weapon is the rocket launcher. Pharah's only weapon is the rocket launcher. Both characters have melee capabilities. Soldier may carry one more weapon but will more often be using something else.
* Soldier rocket jumps by shooting at his own feet. You can shoot against a wall to gain horizontal momentum and you can crouch mid air to gain more height. A more advanced technique wherein you tap your crouch button and time the release with the rocket explosion will propel you even further at the cost of more health. All in all, Soldier's cost for rocket jumps are health and ammunition. Better mechanical skill will make it possible for you to get further with less ammunition and health spent. Pharah *can* do traditional rocket jumps, but it doesn't get you much momentum. Instead, you use an ability to propel yourself up into the air. For horizontal momentum, you can use her concussion blast to knock yourself back. This is another ability. In both these cases, there is no investment other than getting a cooldown on the ability.
* To knock people back, Soldier uses his rockets. Placing your rockets well allows you to move opponents into unfavourable positions, e.g. shooting somewhat behind a medic will bring him closer to you. Phara's knockback from basic shots was simplified by Blizzard a while after release. While there is some knockback, it's rather insignificant. It is most beneficial to always aim for direct hits. If you want knockback with Pharah, her concussion blast is, as mentioned previously, an ability with a cooldown.
* One thing that Pharah has inherently is her jetpack and of course, her Rocket Barrage ultimate. The closest you can get to this in Team Fortress would be The Base Jumper secondary weapon, which allows you to slow your fall speed. This was released at the same time as The Air Strike, a rocket launcher that builds up a larger clip for each kill you get without dying, and firing faster mid-air. These two were intended to be combined, of course, and the result is actually rather similar to Pharah. The differences of course being that a) soldier can't gain altitude with his parachute and b) The Air Strike rewards kill streaks, whereas Pharah's ultimate is more forgiving as you don't lose it on death (and you don't get to keep it after firing it!)

*As a disclaimer, I've played 50+ hours of both Soldier and Pharah in their respective games, so I would consider myself qualified to have an opinion but I am by no means some kind of authority on these kinds of things.*

All that said, I think it's pretty fair to say that we can conclude that Soldier pretty much has the same options as Pharah, yet he does almost all of it with just his primary weapon and has loads of room for improving your mechanical skills whereas Pharah has her abilities and that's almost it.

You might counter my argument with the fact that Soldier's parachute is just a click to activate ability, and that's right. So is Pharah's jetpack, and it runs on fuel which introduces management, which is interesting but itself is another topic. Soldier's parachute was introduced in 2014, seven years after the game's release.

On a similar note, Spiral Knights started adding cooldown based abilities post release. The first was the dash and bash, the former just being a quick hop which rendered you temporarily invincible. Simple stuff, on a cooldown. The shield bash wasn't quite a cooldown based ability. It just drained your shield bar about halfway, if I recall correctly. Pretty similar to Pharah's jetpack in that regard again.

The second introduction of cooldown based abilities was with Battle Sprites. Players frequently requested pets for the game (as in, every other suggestion post asked for pets. The others asked for dual wielding), and they were eventually implemented in the game's biggest update ever, which made the game free-er to play (removed cost of playing each stage) and rehauled various progression systems. Battle Sprites are however, not really pets. They're flying creatures that can use a cooldown based ability *as if you were using it youself*. Almost all their abilities are fire and forget, either in the form of shooting from your position, adding a buff to you, or debuffing close by enemies. The one skill I have to give credit to for having the sprite act as its own entity is Seraphynx's laser, which makes the divine kitten stay in place and shoot a laser with a strong knockback for a few seconds. This was intended to make it a kind of wall that enemeies couldn't pass as I understood it, but it wasn't implemented all too well.

To get to the point, abilities are boring and mechanics are fun and allow for creativity. Let's make some examples using Pharah: her passive jetpack (activated with space-mid air) that uses fuel could be combined with her jetpack ability (activated with shift) which propels her far up in the air and runs on a cooldown. If these two were combined and both used her fuel tank, it would allow for more creative options:

* If you get to high ground and have a full jetpack, you could have more air-time as you'd have the passive jetpack for longer.
* If you need a quick boost to take a sneak peek at the enemy team or throw a few shots at them while they're far away, you could boost twice to get the necessary altitude and then fall down as you'd be out of fuel.

Let's go further with the thought experiment and remove the concussion blast as well. To do that, we'll need to split it up. As it works in game, it can affect yourself, almost exclusively used for providing horizontal momentum, and for knocking back enemies. We can move the former into her jetpack and have it use fuel, while simply tweaking her primary rockets' knockback to have roughly the same potential as the Team Fortress Soldiers'.

All that done, we have redesigned Pharah to have no cooldown based abilities and allow for more creativity and fun in her use. This kind of design is why I consider cooldowns to be lazy design. It's not hard to do better, yet it's present and centric even in our highest budget games and with the recent and upcoming team based shooters (Overwatch, Paladins, Lawbreakers, Quake Champions, etc.), I find it's a worrying trend. It limits the potential depth of a game and turns into a sort of meta-game wherein success is tied to one's ability to keep track of timers.

Let's take a Lockdown example, the dash. In its current state, it's pretty much just used as an escape used in order to stall after running out of striker boost. We should tie it to another resource, for example being an emergency boost that is automatically used once you're out of regular boost and hit the boost button one more time, or it could simply cause an explosion in your back, costing you a little bit of health instead of being reliant on regular boost. This doesn't apply to all Lockdown classes, nor would it be relevant to Spiral Knights PvE, but these are the kinds of changes I want to make in Project Lockdown.

If you haven't figured out how this relates to Project Lockdown, it's very simple: **no bloody cooldowns.**
