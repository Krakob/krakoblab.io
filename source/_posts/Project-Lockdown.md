---
title: Project Lockdown
date: 2017-07-17 05:27:51
tags: 
 - Project Lockdown
 - Game design
 - Game development
 - Spiral Knights
categories:
 - Project Lockdown
---
If you're actually reading this blog, I'll assume you know me as a friend or acquantance through one of the communities I participate in, one possibility being Spiral Knights. Or at least, I used to participate in it. Nowadays I mostly spend time on the [The Arcade Discord server](https://discord.me/spiralknights).

For those of you who don't know, Spiral Knights is a massively multiplayer online action roleplaying game featuring cute little futuristic knights who colonise the planet Cradle. It's a top notch game!

More specifically, Lockdown, one of its two player vs player modes¹, is a top notch game. It's also a terrible game. But I've never found a better game, and I've never found a worse game.

Lockdown is, for lack of better (or worse) words, The Legend of Zelda meets League of Legends meets cocaine. Your team consisting of four to six players compete against the other team to hold three or five control points across the map, each held control point giving your team points over time. The first team to reach 500 or 900 points (three points or five points respectively) wins the game.

As you may have guessed, you'll also participate in murder of the opposing team. The game features three weapon classes: swords, guns, and bombs. Theoretically, it's somewhat of a rock-paper-scissors scenario:

* Bombs counter swords, as a sword user cannot approach a bomb safely.
* Guns counter bombs as a gunslinger can stand at a safe distance while the bomb user has to wait for the fuse of each placed bomb, rendering the bombs harmless
* Swords kind of counter guns, as guns are harder to handle at close range and inflict less damage than swords do. This is the weakest counter as there is no hard countering involved.
  
All in all this balance is largely outweighed by mechanical skill, not to mention that each player can use any combination of weapons they like (the most common being swords + guns).

Lockdown also features three classes. In regular Spiral Knights gameplay, the player is equipped with a shield that doesn't do all that much other than shielding incoming attacks. Lockdown's classes replace your shield with far more powerful abilities activated with the shield button:

* Striker: by far the most popular and iconic to Lockdown, it increases your movement speed by a huge amount. Spiral Knights features a shield cancelling mechanic, making the striker class able to dish out a huge barrage of attacks for as long as they have boost to trigger the shield cancelling with. If they run out, they are of course left defenseless.
* Guardian: most similar to regular Spiral Knights gameplay; gives you a rather potent shield that can also apply a shield to teammates and will heal anyone affected by it (including yourself) at a low rate. This is the only way to regenerate health in Lockdown and thanks to the shield regenerating (unlike health), Guardian is the only class that provides any form of sustainability. Combined with a guardian, a striker can play more aggressively as they can afford to take hits onto their guardian's shield while the opposing striker can't (unless they too have a guardian with them).
* Recon: in my opinion the most versatile class. The shield is replaced with an invisibility cloak. This puts full focus on mind games between you and your opponent during combat, as your survival is entirely dependent on whether you can fool your opponent (presumably a striker who's a lot faster than you) with your footwork. Since being invisibile is kind of powerful, the game counteracts this by implementing a recon mark which applies to any opponent inside a larger radius. This mark serves two purposes: to alert the opposing player of the recon's presence and to lower the defence of the opposing player, making a sneaky attack more potent. If the opponent stays within the recon's radius for a couple of seconds, the recon mark will change into a death mark, removing *all* defences the opponent has, which can make them possible to kill in as little as two strikes.

There's a good bit more nuance to it, but I shan't go into too many details. Let me instead explain what makes Lockdown a good game:

* Lockdown is fast paced. VERY fast paced. While not as complex, I would definitely say it's about as fast as Super Smash Brothers Melee.
* Each class feels very powerful. It's a bit of balance through making everthing overpowered. On paper, each class sounds pretty ridiculous and in practice, they really are! Of course, that is provided that the player has the mechanical skills for it, or at least better mechanical skills than their opponents.
* Lockdown is responsive. I like to think of character controls as a scale between RTS and something like The Legend of Zelda. In an RTS, you select a unit and tell it where to go or what to do. They'll figure it all out. In a MOBA, you tell your character what to do or where to go. They'll figure out details like pathfinding or placing themselves close enough to an enemy in order to hit it. In Zelda, you just do what you want to do or go where you want to go. My issue with MOBAs and even games that edge closer to direct control, like Awesomenauts and Battlerite is that they place too close to the MOBA model. Lockdown is spot on with the Zelda model of things, making the game control amazingly and putting the ceiling for mechanical skill very high.

Let's also talk about why Lockdown is a bad game:

* The entry barrier is very high and veteran players will have access to stronger gear, making them not only have the advantage of experience, but also numeric advantages (particularly in terms of health, which you can nearly double with so called trinkets).
* Spiral Knights is at this point a pretty old game, running on its own Java based engine. It features little compensation for latency, giving a strong advantage to low ping players and even being the detrimental factor in differing between top players. All in all it's just a bit of a technological mess.
* The queue system for Lockdown is terrible. Each game takes about 8 minutes and you can only join between games (unless there's at least 8 people to start a new lobby with). There is no way to queue with friends (other than guild vs guild lockdown, which requires 12 players in total) and there's no matchmaking of any kind.
* Spiral Knights is balanced around its PvE content. Lockdown is at large an afterthought and has as a result gained virtually no balance patches since its release many years ago.
* There's only one game mode. Players need to arrange with each other in order to host duels, for example. Due to the small playerbase, 1v1 tournaments are the most common competitions in the community.

So, that's Lockdown for you. Now you might be wondering what I'm trying to get to. The real answer is that this post was written after spending about 20 hours awake while inside an airplane with nothing better to do and no ability to sleep. The fake answer is that I have a vision for a game, which I call Project Lockdown. There is a thousand things I would change with Lockdown and a thousand others that I wouldn't touch. In this series of posts, I will analyse Lockdown in its original form, propose new ideas to make a more interesting game, and maybe, just maybe, one day I'll actually make a game out of it.

---

¹ The second PvP mode in Spiral Knights is called Blast Network. It's based on Bomberman but with lag which could be counteracted due to the extremely predictable nature of how bombs act. Pretty much the only time I found myself enjoying Blast Network was one time while on voice chat with my sister with another mister and just bantering while throwing a Spark of Life (effectively a pay to lose item, as the strongest strategy in Blast Network is to place as few bombs as possible while hoping the enemy team self destructs more than you do) every time I died.
