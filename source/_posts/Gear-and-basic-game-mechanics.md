---
title: Gear and basic game mechanics
date: 2017-07-24 18:55:49
tags:
 - Project Lockdown
categories:
 - Project Lockdown
---

A core part of Lockdown was of course designing your loadout. You'd tailor it to your preferred weapons and use the armour that best countered your average enemy. Common among hero based MOBAs, however, is that items are purely statistical improvements such as higher damage, magic resistance, and so on. Weapons aren't a choice at all as you're bound to your hero's abilities. This is rather boring, as any countering is on a hero vs hero basis, and given their drastically different functions, matchups can be very predictable.

In Lockdown, countering another player would instead be as simple as switching out a weapon to counter theirs, use an armour that resists their statuses, or simply avoiding engaging them.

That said, Lockdown had pretty boring armours that often did little to make a difference. Players frequently suggested new properties instead of rebalancing. Here are a few ideas in the same spirit that could be implemented in Project Lockdown gear:

* Immune to knockback
* Immune to slowdown
* Kills grant temporary buffs in movement speed
* Significant gains in stats (not actually that interesting, but impactful, unlike Lockdown armour)

I think you get the idea. Here's my idea of equipment slots, subject to change of course:

* 1x ability trinket, allows you to cast a utility ability. Needs to be channeled for a few seconds, generally useful outside of combat. For example, teleport to a player on your team, heal up a bit, summon a mount, etc.
* 1x passive trinket, grants offensive buffs.
* 1x passive armour, grants defensive bonuses (but no plain less damage or more health kind of stuff)
* 1x class slot, determines your health and two abilities. One channeled, like Recon Cloak, and one activated, like Shield Bash.
* 2x weapon slots, dual wielded at all times. All weapons have two forms of attack, but no charge attacks.

All in all that's three passive items (counting class) and seven activated. Since I'm getting ahead of myself, let's talk weapons and class abilities.

In Spiral Knights, each weapon has a regular attack (combo for swords, magazine for guns, and a dud for bombs) and a charge attack (either useless or cheap for swords, useless for guns, and the only way to use a bomb). To turn this into a better system, interesting charge attacks can adapted to a secondary attack while other attacks can be reworked. Valiance could for example get a melee alternative attack since it has a bayonett on it. Some swords could get a block or a counter. Maybe reflecting projectiles! Bombs are a bit trickier in this regard, but could get options such as lobbing, rolling, and placing uncharged bombs. Maybe even fake bombs just to bring those mind games to the next level!

My suggestion for classes are pretty much the same, so I'll be quicker on that:
* Striker can have boost and dash
* Guardian can have shield and bash
* Recon can have invisibility cloak and death mark (recon mark only applies while recon is invisible but still has an effect)
