---
title: A map of Kyuss and related acts
date: 2018-04-19 14:19:51
tags: [music]
---

For a long while, I've wanted to make a deep dive in the history of Kyuss, one of my absolutely favourite bands by making a chart of them and related acts. Turns out I took on a larger task than I'd anticipated, but I saw it through. The results are available below:
<escape><!-- more --></escape>

{% asset_link "source.xml" "Draw.io source XML" %} | {% asset_link "diagram.png" "PNG" %} | {% asset_link "diagram.svg" "SVG" %} | {% asset_link "diagram.pdf" "PDF" %}

{% asset_image "diagram.png" "A map of Kyuss and related acts" %}
