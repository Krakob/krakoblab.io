---
title: Wishlist
date: 2017-07-24 18:51:01
tags:
 - Project Lockdown
categories:
 - Project Lockdown
---

A list of features the game should have, in no specific order:

 * Optional steam integration w/ trade, joining, et cetera.
 * Built in voice chat
 * Fully open source, including underlying technology. The trickiest of these being game engine. One contender is Godot, but it might not be mature enough.
 * Running on Vulkan (or OpenGL) for easy porting
 * Binaries distributed in AppImage format for Linux.
 * Official town square servers for SK nostalgia
 * No purchased lootboxes or other gambling bullshit
 * No RNG
 * No cooldowns
 * Models are solid objects so that they can be 3d printed!
 * Supports arbitrary refresh rates, resolutions (requires a well scaling UI), and aspect ratios (requires a fog of war).
 * Servers, too, support arbitrary refresh rates. 60Hz is a minimum for official servers, but 120Hz should be the target.
