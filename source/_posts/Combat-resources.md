---
title: Combat resources
date: 2017-07-24 20:44:21
tags:
 - Project Lockdown
categories:
 - Project Lockdown
---
In my former post about cooldowns, I may have failed to explain my idea of resources a bit. You may have asked, what would the effective difference between a shield that regenerates on a set cooldown and a shield that regenerates over time be?

In many cases, these differences aren't all too big. All in all it's different kinds of resources. A player might have the following resources in various games:

* Cooldowns
* Ammunition (total)
* Current clip (ammunition might be infinite)
* Over-time regeneration

They might also have more abstract resources:

* Position
* Motion. If they're motionless when attacking, they're left vulnerable when attacking.
* Teammates in their vincinity

The reality of game design is that you can't really avoid passive, numeric resources entirely. But there are interesting resources that allow for player creativity and resources that merely serve to limit the player. This is one of my core ideals in Project Lockdown.
