---
title: Pedalboard build log
date: 2018-06-14 12:28:03
tags:
  - Music
  - Maker stuff
---

Here's a log of a pedalboard I built. I wanted to upgrade due to lacking space on my old Pedaltrain Metro, seen below, that could barely fit half of my pedals. Not to mention I wanted some funky features as well.

{% asset_img 00.jpg Old pedalboard %}

This is the initial drawing I made. The aim was about 1m × 0.5m with rack mounting in the back for a patch panel, DI, and drawer. In the end I opted not to pick up a DI since I don't have an interface to make use of it yet either way.

{% asset_img 01.jpg First render %}

After picking up material, I adjusted the drawing to those dimensions. The large bottom parts are birch while the rest is pine. I'd have loved to use oak but it's rather expensive in comparison and I was going to put on a nice finish anyway.

{% asset_img 02.jpg Second render %}

Spent a lot of time cutting and milling with a table router. I didn't take many pictures at this point. I unfortunately made a couple of mistakes: I counted the birch pieces as being 15mm thick, though they were actually 18mm. I also cut the planks a little bit short. This would be a problem for the rack mounting as I probably lost almost a centimeter of width. My solution to this was to mill about 2mm into the birch pieces where the rack rails would go. This got me a total of 8mm back and turned out to be just enough in the end. It also looked a bit better, as the rails are now flush with the rest of the pieces. Though it's hardly visible since it's on the bottom and the back of the board.

{% asset_img 03.jpg Patch bay assembly %}

{% asset_img 04.jpg Test-fitting %}

{% asset_img 05.jpg Mmmm corners %}

{% asset_img 06.jpg Gluing the frame together. Had to force the angles a bit %}

{% asset_img 07.jpg Adding the middle support piece and the planks %}

{% asset_img 08.jpg This certainly held them down %}

{% asset_img 09.jpg After letting the glue dry%}

{% asset_img 10.jpg Painting the bottom %}

{% asset_img 11.jpg And the top %}

{% asset_img 12.jpg And basically done! %}

{% asset_img 13.jpg %}

{% asset_img 14.jpg %}

{% asset_img 15.jpg Adding furniture pads %}

{% asset_img 16.jpg Using the patchbay put on backwards to place the rails correctly %}

{% asset_img 17.jpg At this point we had to move inside due to the mosquitoes. Anyhow, it worked! %}

{% asset_img 18.jpg Equipped with brand name super velcro %}

{% asset_img 19.jpg And all done! %}

{% asset_img 20.jpg Drawer in action %}

{% asset_img 21.jpg Patch bay in use %}

{% asset_img 22.jpg Done for real here. I made almost all these cables myself, that took a good while. I'd say it saved me a lot of money but I mean if I wanted to save money I'd just not have done this in the first place. %}

{% asset_img 23.jpg Cat tax %}
