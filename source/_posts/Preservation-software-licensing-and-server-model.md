---
title: 'Preservation, software licensing, and server model'
date: 2017-07-24 15:22:53
tags:
 - Project Lockdown
 - Spiral Knights
 - Team Fortress 2
 - Overwatch
categories:
 - Project Lockdown
---
The preservation of information has been important to humankind for almost as long as society has existed. It is simply an effective way of teaching others and spreading knowledge. Today, it's of course used for many other things. It can be as simple as videotaping a standup comedian so that others can enjoy their show even if one cannot attend the show. Although this kind of preservation is not necessarily that important, it's the preservation of culture and without our culture, we aren't all that interesting.

In terms of videogames, preservation has historically not been a significant challenge. To preserve an NES game, you simply need an adapter to read the cartridge's data and a storage medium to keep it on. Using the internet, you can spread it far and wide. And thus, the game in question will be incredibly hard to truly lose forever. There's of course the question of piracy (and then, DRM), but that's another topic.

With games having gone online, there's always at least two parts involved in the game. Thanks to shoddy internet connections and server costs, these two parts were usually players. This does not hold up today though, as it requires mutual trust between the parts (cheating might be easy), requires a good connection between players (easy through LAN, harder through the Internet), and does not scale properly (total amount of connections in a session equals n factorial, whereas a server model simply has n connections).

The server model is pretty much a given today, as it allows for features like matchmaking, inventories, cheat prevention, social networks, etc. Even games played over LAN often use a server model (e.g. Terraria) simply because the developers won't have to develop two multiplayer types and PCs today are often powerful enough to run games and a smaller server in parallel.

The only real disadvantages of a server model today is that the server is proprietary code and inaccessible. When you buy Overwatch, you really just buy a license to connect to their servers. You don't own the game, you can't host your own server, Blizzard can ban you, effectively revoking your $40 license to play Overwatch. With a proprietary and remote server, there's simply no way to own a game and thus, there's no way to preserve it, save for illegally accessing the server software or writing your own substitute server. You're at the mercy of the developer.

A game that handles this issue pretty well and that I'll in part model Project Lockdown after is Team Fortress 2. Valve really nailed the game's model, using a central server for inventories, profiles, and matchmaking and several game servers to play on. The game server software is available to the players, allowing regions with poor connection to official servers to host their own and those looking for fun to mod the game without compromising the player's account's security. Without the master server, the game can still connect to the individual game servers by inputting their IP. This can of course be abstracted with the help of domains and the `steam://` URI protocol. All in all there's no major issues with preserving Team Fortress 2.

This kind of preservation has one flaw, however, and that's closed source software. While you could keep the TF2 client and server binaries, you can not host your own master server and you can't modify the game without reverse engineering. Should Valve decide to close down the TF2 servers, only part of the game can be preserved.

## Open source commercial games?

Most people would probably say that this is nuts because a) people can then hack the game and b) then how do you make money off of it?

On the subject of a), it's actually pretty darn simple. Just like a website needs to protect itself by implementing server side checks against vulnerabilities and not blindly trust a user who says "hey server, I'm totes magotes an administrator and I'll need you to hand me all your sensitive information", a game also needs to protect itself in the same manner. Coincidentially, Spiral Knights is pretty good in this regard since it has little prediction logic implemented, leaving nothing to trust the user as far as I know. This would be pretty bogus in say, an FPS as you'd expect to hit if you click when your crosshair is lined with the opponent. Spiral Knights has only projectiles and swing animations, which mitigates this issue somewhat, and would do so even more with some basic client side predictive logic.

As for b), we can actually look to Red Hat! They make an open source operating system and manage to make money off of it via selling support and the license to download binaries (as opposed to having to compile yourself, or relying on other parties to provide binaries). Project Lockdown can be monetised by having the purchased license include the ability to download binaries, connect to the official master server, and get support through official channels. Cosmetics can also be sold on the official master server as a means of supporting sustained development.
